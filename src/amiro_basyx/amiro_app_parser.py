import re
import argparse
import sys


# ---------- PARSING ----------
# results are ordered! -> position = topicid
def _general_configuration_parse(file_path: str, ENUM_TOKEN: str, ENUM_LINE_START: str) -> list[tuple[str, str, str]]:
    result = []
    with open(file_path) as f:
        for line in f:
            if ENUM_TOKEN in line:
                break
        else:
            assert False, "There should be an enum in this file"
        for line in f:
            if ENUM_LINE_START in line:
                r_result = re.search(r'\s*(\w+),?\s*/\*\*<\s*(.*)\*/\s*/\*payload=(.*)\*/', line)
                assert r_result, f"Regex doesn't match {line}"
                result.append((r_result.group(1), r_result.group(2), r_result.group(3),))
            elif '};' in line:
                break
            else:
                assert False, f"Unexpected {line}"
    return result


def parse_configuration_topics(file_path: str) -> list[tuple[str, str, str]]:
    return _general_configuration_parse(file_path, ENUM_TOKEN='enum', ENUM_LINE_START='TOPICID_')


def parse_configuration_services(file_path: str) -> list[tuple[str, str, str]]:
    return _general_configuration_parse(file_path, ENUM_TOKEN='enum services', ENUM_LINE_START='SERVICEID_')


def parse_APP(file_path: str) -> tuple[list[str], list[str]]:
    # services an app provides (over can)
    services = []
    # topics an app provides (over can)
    topics = []
    with open(file_path) as f:
        for line in f:
            # Find services
            r_result = re.search(r'\s*service_IDS\[\d+\]\s*=\s*(\w+);', line)
            if r_result:
                services.append(r_result.group(1))
            # Find topics (this skips comments!)
            r_result = re.search(r'\s*subscriber_topic_ids\[\d+\]\s*=\s*(\w+);', line)  # confusion: pub_topic_IDs
            if r_result:
                topics.append(r_result.group(1))
    return (services, topics)


def main(arguments: list[str] = sys.argv[1:]) -> None:
    argparser = argparse.ArgumentParser()
    argparser.add_argument('--modules', default=['DiWheelDrive_1-2', 'LightRing_1-2', 'PowerManagement_1-2'])
    argparser.add_argument('--configurations-root', default='AMiRo-Apps/configurations/')
    argparser.add_argument('--configuration', default='AMiRoDefault')
    args = argparser.parse_args(arguments)

    if args.configurations_root.endswith('/'):
        args.configurations_root = args.configurations_root[0:-1]

    config_url = "https://gitlab.ub.uni-bielefeld.de/AMiRo/AMiRo-Apps/-/tree/main/configurations"

    configuration_root = f"{args.configurations_root}/{args.configuration}"

    configuration_topics = parse_configuration_topics(f"{configuration_root}/modules/AMiRoDefault_topics.h")
    configuration_services = parse_configuration_services(f"{configuration_root}/modules/AMiRoDefault_services.h")
    apps = {}
    for module in args.modules:
        app_path = f"{configuration_root}/modules/{module}/apps.c"
        services, topics = parse_APP(app_path)
        apps[module] = {"services": services, "topics": topics}

    # ---------- CONVERT TO Basyx Properties ----------

    from basyx.aas.model import (
        Asset, AssetAdministrationShell, Submodel, SubmodelElementCollectionUnordered as SECUnordered, Property,
        Capability, datatypes, Identifier, IdentifierType, AASReference, Reference, Key, KeyElements, KeyType
    )
    from basyx.aas.model.datatypes import String, Int
    from basyx.aas.model.base import AssetKind

    CAP_SM_BASE_URL = 'https://wiki.eclipse.org/BaSyx_/_Documentation_/_Submodels_/_Capability'
    CAP_PropertyContainer = Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                           local=False,
                                           value=f'{CAP_SM_BASE_URL}#PropertyContainer',
                                           id_type=KeyType.IRI
                                           ),))
    CAP_PropertySet = Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                     local=False,
                                     value=f'{CAP_SM_BASE_URL}#PropertySet',
                                     id_type=KeyType.IRI
                                     ),))
    CAP_CapabilityContainer = Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                             local=False,
                                             value=f'{CAP_SM_BASE_URL}#CapabilityContainer',
                                             id_type=KeyType.IRI
                                             ),))
    CAP_CapabilitySet = Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                       local=False,
                                       value=f'{CAP_SM_BASE_URL}#CapabilitySet',
                                       id_type=KeyType.IRI
                                       ),))

    board2board_id = {'DiWheelDrive_1-2': 0,
                      'PowerManagement_1-2': 1,
                      'LightRing_1-2': 2
                      }

    p_id = Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                          local=False,
                          value=f'{CAP_SM_BASE_URL}#Property',
                          id_type=KeyType.IRI
                          ),
                      ))
    caps = []
    for topic_id, (topic_name, topic_desc, topic_payload) in enumerate(configuration_topics):
        topic = {"id": Property(id_short="id", value_type=Int, value=topic_id, semantic_id=p_id),
                 "name": Property(id_short="name", value_type=String, value=topic_name, semantic_id=p_id),
                 "description": Property(id_short="description", value_type=String, value=topic_desc, semantic_id=p_id),
                 "payload": Property(id_short="payload", value_type=String, value=topic_payload, semantic_id=p_id),
                 "type": Property(id_short="type", value_type=Int, value=0, semantic_id=p_id)  # PubSub
                 }
        for m in args.modules:
            if topic_name in apps[m]['topics']:
                topic["origin_board_id"] = Property(id_short="origin_board_id", value_type=Int, value=board2board_id[m],
                                                    semantic_id=p_id)
                topic["support_can"] = Property(id_short="support_can", value_type=Int, value=1, semantic_id=p_id)
        caps.append(topic)

    for service_id, (service_name, service_desc, service_payload) in enumerate(configuration_services):
        service = {"id": Property(id_short="id", value_type=datatypes.Int, value=service_id, semantic_id=p_id),
                   "name": Property(id_short="name", value_type=String, value=service_name, semantic_id=p_id),
                   "description": Property(id_short="description", value_type=String, value=service_desc,
                                           semantic_id=p_id),
                   "payload": Property(id_short="payload", value_type=String, value=service_payload, semantic_id=p_id),
                   "type": Property(id_short="type", value_type=Int, value=1, semantic_id=p_id),  # Service
                   }
        for m in args.modules:
            if service_name in apps[m]['services']:
                service["origin_board_id"] = Property(id_short="origin_board_id", value_type=Int,
                                                      value=board2board_id[m], semantic_id=p_id)
                service["support_can"] = Property(id_short="support_can", value_type=Int, value=1, semantic_id=p_id)
        caps.append(service)

    # ---------- BUILD AAS ----------

    config_asset = Asset(kind=AssetKind.INSTANCE,
                         identification=Identifier(f'{config_url}/{args.configuration}/asset', IdentifierType.IRI),
                         id_short=f"amiro_{args.configuration}",
                         description={"language": "en", "text": "test"}
                         )

    # Generate Capabilities:
    capabilities_container = []
    for cap in caps:
        properties = []
        for property_name, property_value in cap.items():
            tmp = SECUnordered(semantic_id=CAP_PropertyContainer, id_short=property_name, value=[property_value])
            properties.append(tmp)
        propertySet = SECUnordered(semantic_id=CAP_PropertySet, id_short="PropertySet", value=properties)
        capability = Capability(semantic_id=CAP_CapabilityContainer, id_short=cap["name"].value)  # semantic_id wrong?
        tmp = SECUnordered(semantic_id=CAP_CapabilityContainer, id_short=cap["name"].value,
                           value=[capability, propertySet])
        capabilities_container.append(tmp)

    cap_set = SECUnordered(semantic_id=CAP_CapabilitySet, id_short="CapabilityContainer", value=capabilities_container)

    capabilities = Submodel(identification=Identifier(f'urn:{args.configuration}:capabilities', IdentifierType.IRI),
                            id_short="capabilities",
                            semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                       local=False,
                                                       value=CAP_SM_BASE_URL,
                                                       id_type=KeyType.IRI
                                                       ),)),
                            submodel_element=[cap_set]
                            )

    # should be AASReference.from_referable(asset) but does not work
    amiro_config_aas = AssetAdministrationShell(asset=config_asset,
                                                identification=Identifier(f'{config_url}/{args.configuration}',
                                                                          IdentifierType.IRI),
                                                id_short=f"amiro_{args.configuration}",
                                                submodel={AASReference.from_referable(capabilities)}
                                                )

    # ---------- Upload ----------
    from basyx.aas.adapter.json.json_serialization import AASToJsonEncoder
    import json
    import requests
    import urllib

    baseurl = 'http://pyke.techfak.uni-bielefeld.de:9000/' # pyke.techfak.uni-bielfeld.de
    auth = None  # HTTPBasicAuth(self.user, self.password)
    headers = {"Content-Type": "application/json"}

    session = requests.Session()
    session.auth = auth
    session.headers.update(headers)
    aas_json_str = json.dumps(amiro_config_aas, cls=AASToJsonEncoder)
    submodel_json_str = json.dumps(capabilities, cls=AASToJsonEncoder)
    key = urllib.parse.quote(amiro_config_aas.identification.id, safe="")

    response = session.put(f'{baseurl}aasServer/shells/{key}', data=aas_json_str)
    if response.status_code != 200:
        print('PUT request failed with status code:', response.status_code)
        print('PUT request failed with status message:', response.text)
    else:
        print(f"Pushed AAS: {amiro_config_aas.id_short}")

    response = session.put(f'{baseurl}aasServer/shells/{key}/aas/submodels/capabilities', data=submodel_json_str)
    if response.status_code != 200:
        print('PUT request failed with status code:', response.status_code)
        print('PUT request failed with status message:', response.text)
    else:
        print(f"Pushed SubModel: {capabilities.id_short}")

    # ->CapabilitySet
    # ->->CapabilityContainer
    # ->->->LightService (Cap)
    # ->->->PropertySet
    # ->->->->PropertyContainer
    # ->->->->->idShort: Type; value: Service --> Property
    # ->->->->PropertyContainer
    # ->->->->->idShort: id; value: 1 --> Property
    # ->->->->PropertyContainer
    # ->->->->->ShellCMD
    # ->->->->PropertyContainer
    # ->->->->->Canbridge
    # ->->->->PropertyContainer
    # ->->->->->Payload: ToCHeader:Type


if __name__ == '__main__':
    main(sys.argv[1:])
