import sys
import argparse
from basyx.aas import model
from basyx.aas.model import Reference, Key, KeyElements, KeyType, AASReference
from ml4proflow_mods.basyx.connectors import BasyxRegistryConnector, BasyxAASShellConnector

def main(arguments: list[str] = sys.argv[1:]) -> None:
    init_parser = argparse.ArgumentParser(description='Manage the AMiRo digital twin', formatter_class=argparse.ArgumentDefaultsHelpFormatter, add_help=False)
    init_parser.add_argument('--board-names', default=["LightRing", "PowerManagement", "DiWheelDrive"] , help="The order is top to bottom!", nargs='+')
    args, extra = init_parser.parse_known_args()

    parser = argparse.ArgumentParser(description='Manage the AMiRo digital twin', formatter_class=argparse.ArgumentDefaultsHelpFormatter, parents=[init_parser])
    parser.add_argument('--aas-url', default='http://pyke.techfak.uni-bielefeld.de:9000/') #
    parser.add_argument('--auth', default=None, help='not implemented yet')
    parser.add_argument('--iri-suffix', default='')
    parser.add_argument('--amiro-asset-description', default='TBD')
    parser.add_argument('--configuration', default='AMiRoDefault')

    # Only jpg is currently supported

    defaults = { # all boards need the same keys!
                    "LightRing": {"id":59, "hardware-version": "1.2", "firmware-version": "d847a23d4a82","software-version": "7a90faf06474", "manufacture-year": 2019, "manufacture-month": 12, "manufacture-day": 20, "year-of-construction": "2012","side-view-path": "images/lightring_sideview"},
                    "PowerManagement": {"id":59, "hardware-version": "1.2", "firmware-version": "d847a23d4a82","software-version": "7a90faf06474", "manufacture-year": 2019, "manufacture-month": 12, "manufacture-day": 20, "year-of-construction": "2012", "side-view-path": "images/lightring_sideview"},
                    "DiWheelDrive": {"id":59, "hardware-version": "1.2", "firmware-version": "d847a23d4a82","software-version": "7a90faf06474", "manufacture-year": 2019, "manufacture-month": 12, "manufacture-day": 20, "year-of-construction": "2012", "side-view-path": "images/lightring_sideview"},
                }

    for board_name in args.board_names:
        for key, default in defaults.get(board_name, defaults['DiWheelDrive']).items(): # every default board should have the same keys so it doesn't matter which one we choose 
            parser.add_argument(f'--{board_name}-{key}', default=default, help="-")

    args = parser.parse_args()
    dargs = vars(args)
    aas_server = BasyxAASShellConnector(args.aas_url)
    
    # Generate Robot Asset & AAS
    amiro_id = args.DiWheelDrive_id
    base_iri = f'http://amiro{amiro_id}{args.iri_suffix}/basyx'

    amiro_asset = model.Asset(kind=model.base.AssetKind.INSTANCE,
                              identification=model.Identifier(f'{base_iri}/robot/asset', model.IdentifierType.IRI),
                              id_short=f"amiro_{amiro_id}",
                              description={"language":"en", "text":args.amiro_asset_description}
                              )

    board_nodes = []

    for board_name in args.board_names:
        board_id = board_name + '_' + str(dargs[f"{board_name}_id"])
        # TODO_ below
        board_asset = model.Asset(kind=model.base.AssetKind.INSTANCE,
                                  identification=model.Identifier(f'{base_iri}/{board_id}/asset', model.IdentifierType.IRI),
                                  id_short=board_id,
                                  description={"language":"en", "text":"test"}
                                  )

        contactInformation = model.SubmodelElementCollectionUnordered(id_short="ContactInformation",
                                                                      semantic_id=model.Identifier('https://admin-shell.io/zvei/nameplate/1/0/ContactInformations/ContactInformation', model.IdentifierType.IRI),
                                                                      value=[model.MultiLanguageProperty(id_short="Street", value={"DE":"Inspiration 1"}),
                                                                             model.MultiLanguageProperty(id_short="Zipcode", value={"DE":"33619"}),
                                                                             model.MultiLanguageProperty(id_short="CityTown", value={"DE":"Bielefeld"}),
                                                                             model.MultiLanguageProperty(id_short="NationalCode", value={"DE":"DE"})
                                                                            ]
                                                                     )
        assetSpecificProperties = model.SubmodelElementCollectionUnordered(id_short="AssetSpecificProperties",
                                                                           semantic_id=model.Identifier('0173-1#01-AGZ672#001', model.IdentifierType.IRDI),
                                                                           value=[model.Property(id_short="Diameter", value_type=model.datatypes.String, value='dummy'),
                                                                                  #model.submodel.File(id_short="SideView", value="a",  mime_type="image/png")
                                                                                  #model.submodel.Blob(id_short="SideView", value=open('img/DiWheelDrive_side2.png', 'rb').read(), mime_type="image/png"), #f'{board_name}_side_view_path'
                                                                                 ]
                                                                           )
        date_of_manufacture = model.datatypes.Date(year=dargs[f'{board_name}_manufacture_year'], month=dargs[f'{board_name}_manufacture_month'], day=dargs[f'{board_name}_manufacture_day']) 
        board_aas_encoded = aas_server._parse_identification(f'{base_iri}/{board_id}/aas')
        nameplate = model.Submodel(identification=model.Identifier(f'urn:{board_id}:nameplate', model.IdentifierType.IRI), #f'{base_iri}/{board_id}/aas/nameplate
                                   id_short="Nameplate",
                                   semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                       local=False,
                                                       value='https://admin-shell.io/zvei/nameplate/1/0/Nameplate',
                                                       id_type=KeyType.IRI
                                                       ),)),
                                   submodel_element=[model.Property(id_short="URIOfTheProduct", value_type=model.datatypes.String, value=f"{base_iri}/{board_id}/"),
                                                     model.MultiLanguageProperty(id_short="ManufacturerName", value={"EN":"AMiRo Board"}),
                                                     model.MultiLanguageProperty(id_short="ManufacturerProductDesignation", value={"EN":"KS"}),
                                                     contactInformation,
                                                     model.Property(id_short="SerialNumber", value_type=model.datatypes.String, value=board_id),
                                                     model.Property(id_short="YearOfConstruction", value_type=model.datatypes.String, value=dargs[f'{board_name}_year_of_construction']),
                                                     model.Property(id_short="DateOfManufacture",  value_type=model.datatypes.Date, value=date_of_manufacture),
                                                     model.MultiLanguageProperty(id_short="HardwareVersion", value={"EN":dargs[f'{board_name}_hardware_version']}),
                                                     model.MultiLanguageProperty(id_short="FirmwareVersion", value={"EN":dargs[f'{board_name}_firmware_version']}),
                                                     model.MultiLanguageProperty(id_short="SoftwareVersion", value={"EN":dargs[f'{board_name}_software_version']}),
                                                     model.submodel.File(id_short="SideView", value=f"{aas_server.url}/{board_aas_encoded}/aas/submodels/Nameplate/submodel/submodelElements/SideView/File",  mime_type="image/png"),
                                                     model.submodel.File(id_short="TopView", value=f"{aas_server.url}/{board_aas_encoded}/aas/submodels/Nameplate/submodel/submodelElements/TopView/File",  mime_type="image/png"),
                                                     assetSpecificProperties
                                                    ]
                                   )

        board_aas = model.AssetAdministrationShell(asset=board_asset,
                                                   identification=model.Identifier(f'{base_iri}/{board_id}/aas', model.IdentifierType.IRI),
                                                   id_short=board_id,
                                                   description={"en":"test"},
                                                   submodel={AASReference.from_referable(nameplate)}
                                                   )
        
        board_nodes.append(model.Entity(board_name,
                              semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                    local=False,
                                                    value='https://admin-shell.io/idta/HierarchicalStructures/Node/1/0',
                                                    id_type=KeyType.IRI
                                                    ),)),
                              entity_type=model.base.EntityType.SELF_MANAGED_ENTITY,
                              asset = AASReference.from_referable(board_asset),
                              statement=[]
                              ))
        
        aas_server.put_aas(board_aas, [nameplate])
        aas_server.put_file(f'img/{board_name}_side.png', f'{base_iri}/{board_id}/aas', f"aas/submodels/Nameplate/submodel/submodelElements/SideView/File/upload")
        aas_server.put_file(f'img/{board_name}_top.png', f'{base_iri}/{board_id}/aas', f"aas/submodels/Nameplate/submodel/submodelElements/TopView/File/upload")

    #Physical robot structure:
    configuration = args.configuration
    
    configuration_node = model.Entity(id_short="configuration",
                              semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                    local=False,
                                                    value='https://admin-shell.io/idta/HierarchicalStructures/Node/1/0',
                                                    id_type=KeyType.IRI
                                                    ),)),
                              entity_type=model.base.EntityType.SELF_MANAGED_ENTITY,
                              asset = Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                    local=False,
                                                    value=f"https://gitlab.ub.uni-bielefeld.de/AMiRo/AMiRo-Apps/-/tree/main/configurations/{configuration}/asset",
                                                    id_type=KeyType.IRI
                                                    ),)),
                                statement=[]
                                ) # todo haspart
                                

    entityNode = model.Entity(id_short="EntryNode",
                              semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                    local=False,
                                                    value='https://admin-shell.io/idta/HierarchicalStructures/EntryNode/1/0',
                                                    id_type=KeyType.IRI
                                                    ),)),
                              entity_type=model.base.EntityType.SELF_MANAGED_ENTITY,
                              asset=AASReference.from_referable(amiro_asset), 
                              statement=[configuration_node] + board_nodes
                              )

    hierarchicalstructures = model.Submodel(identification=model.Identifier(f'{base_iri}/robot/aas/hierarchicalstructures', model.IdentifierType.IRI),
                                            id_short="HierarchicalStructures",
                                            semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                       local=False,
                                                       value='https://admin-shell.io/idta/HierarchicalStructures/1/0/Submodel',
                                                       id_type=KeyType.IRI
                                                       ),)),
                                            submodel_element=[entityNode,
                                                              model.Property(id_short="ArcheType",
                                                                             value_type=model.datatypes.String,
                                                                             semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                                                       local=False,
                                                                                       value='https://admin-shell.io/idta/HierarchicalStructures/ArcheType/1/0',
                                                                                       id_type=KeyType.IRI
                                                                                       ),)),
                                                                             value="Down")
                                                              ] # todo
                                            )

    configuration_node.statement.add(model.RelationshipElement(id_short="HasPart", first=AASReference.from_referable(entityNode), second=AASReference.from_referable(configuration_node),
                                                                    semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                                    local=False,
                                                                    value="https://admin-shell.io/idta/HierarchicalStructures/HasPart/1/0",
                                                                    id_type=KeyType.IRI
                                                                    ),)),
                                                                    ))
    for bn in board_nodes:
        bn.statement.add(model.RelationshipElement(id_short="HasPart", first=AASReference.from_referable(entityNode), second=AASReference.from_referable(bn),
                                                                    semantic_id=Reference((Key(type_=KeyElements.GLOBAL_REFERENCE,
                                                                    local=False,
                                                                    value="https://admin-shell.io/idta/HierarchicalStructures/HasPart/1/0",
                                                                    id_type=KeyType.IRI
                                                                    ),)),
                                                                    ))

    amiro_robot_aas = model.AssetAdministrationShell(asset=amiro_asset, #amiro_asset,
                                                     identification=model.Identifier(f'{base_iri}/robot/aas', model.IdentifierType.IRI),
                                                     id_short=f"amiro_{amiro_id}",
                                                     description={"en":"test"},
                                                     submodel={AASReference.from_referable(hierarchicalstructures)}
                                                     )
    hierarchicalstructures.parent = amiro_robot_aas
    entityNode.parent = hierarchicalstructures
    aas_server.put_aas(amiro_robot_aas, [hierarchicalstructures])
    print(f"Uploaded amiro_{repr(amiro_id)}")

if __name__ == "__main__":
    main(sys.argv[1:])
