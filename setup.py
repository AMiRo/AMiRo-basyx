from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(
    name="amiro-basyx",
    version="0.0.1",
    author="Christian Klarhorst",
    author_email="cklarhor@techfak.uni-bielefeld.de",
    description="AMiRo APP parser to generate AAS models",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="todo",
    project_urls={
        "Main framework": "todo",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points={
        'console_scripts': ['amiro-app-parser=amiro_basyx.amiro_app_parser:main', ],
    },
    #package_dir={"": "src"},
    package_dir={"": "src"},
    packages=find_namespace_packages(where="src"),
    install_requires=["basyx-python-sdk==0.2.2", "requests"],
    extras_require={
        "tests": ["pytest", 
          "pytest-html",
          "pytest-cov",
          "flake8",
          "mypy"],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    python_requires=">=3.6", # todo
)
